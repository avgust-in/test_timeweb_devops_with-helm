# Helm chart for deploy test_timeweb_devops

Yarn application

helm tree:
```
├── app
├── helm
│   ├── Chart.yaml
│   ├── templates
│   │   ├── deploy.yml
│   │   ├── ingress-controller.yml
│   │   ├── ingress.yml
│   │   └── svc.yml
│   └── values.yaml
└── README.md
```


### Instruction

Set your image in helm var file: values.yaml

To deploy use:

``` helm install your-app-name your-Chart.yml-directory```